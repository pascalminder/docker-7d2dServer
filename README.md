# 7d2d Server in a docker environment

I was inspired by the docker container from [Didstopia 7dt2 server](https://github.com/Didstopia/7dtd-server) which I wanted to install on my server. But somehow the server did not seem to work.
So I tried to create my own Docker container to learn more about how Docker works.
After the Docker image was done I found out, that OMV marks all partitions as non executable, which was the reason why the docker container could not start the 7d2d server.

## Install instructions

### Build image
```
docker build --no-cache -t pascalminder/7d2dServer:latest .
```

### Export image
Used for deploying it without DockerHub on Portainer for example.

```
docker save -o 7d2dServer.tar pascalminder/7d2dServer
```

### Start image
```
docker run -d \
   --name=7d2d \
   -e PUID=1000 \
   -e PGID=100 \
   -e TZ=Europe/Zurich \
   -v /sharedfolders/docker/config/7d2d/config:/home/steam/7d2d/config \
   -v /sharedfolders/docker/config/7d2d/save:/home/steam/.local/share/7DaysToDie/Saves \
   -p 26900-26902:26900-26902 \
   -p 26900-26902:26900-26902/udp \
   pascalminder/7d2dServer:latest
```