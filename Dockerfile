FROM ubuntu:18.04

MAINTAINER Pascal Minder <pascal.minder@trustus.ch>

ARG PGID=1000
ARG PUID=911

# Update the repository
ARG DEBIAN_FRONTEND=noninteractive

RUN dpkg --add-architecture i386 \
   && apt-get update -y \
   && apt-get install -y --no-install-recommends \
      ca-certificates locales \
   && rm -rf /var/lib/apt/lists/*

# Add unicode support
RUN locale-gen en_US.UTF-8

ENV LANG 'en_US.UTF-8'
ENV LANGUAGE 'en_US:en'

RUN set -xe && \
   useradd -u $PUID -U -m steam && \
   usermod -G users steam

ENV PGID $PGID
ENV PUID $PUID

# set up startup environment
ARG INITDIR=/startup

ADD init/setup_users.sh $INITDIR/setup_users.sh
ADD init/startup.sh $INITDIR/startup.sh

RUN set -e \
   && chown -R root:root \
      $INITDIR \
   && chmod -R u+x \
      $INITDIR

## install steamcmd
ARG SCRIPTDIR=/app

ENV STEAMCMDDIR /home/steam/steamcmd
ENV STEAMGAMECONFIGDIR /home/steam/7d2d/config
ENV STEAMGAMESAVEDIR /home/steam/.local/share/7DaysToDie/Saves

RUN mkdir -p \
   $SCRIPTDIR

ADD /scripts/steamcmd_install.txt $SCRIPTDIR/steamcmd_install

# add dependencies
RUN set -x \
   && apt-get update \
   && apt-get install -y --no-install-recommends --no-install-suggests \
      lib32stdc++6 \
      lib32gcc1 \
      wget \
      nano \
   && apt-get clean autoclean \
   && apt-get autoremove -y \
   && rm -rf /var/lib/apt/lists/*

RUN mkdir -p ${STEAMCMDDIR} ${STEAMGAMECONFIGDIR} ${STEAMGAMESAVEDIR} \
   && chown -R ${PUID}:${PGID} ${STEAMCMDDIR} ${STEAMGAMECONFIGDIR} ${STEAMGAMESAVEDIR}

## startup script
ADD scripts/run.sh $SCRIPTDIR/run.sh
ADD scripts/update_check.sh $SCRIPTDIR/update_check.sh

# Fix permissions
RUN chown -R ${PUID}:${PGID} \
   $SCRIPTDIR

# Fix permissions
RUN chmod u+x \
   $SCRIPTDIR/run.sh \
   $SCRIPTDIR/update_check.sh

WORKDIR $INITDIR
ENV SCRIPTDIR=$SCRIPTDIR

ENTRYPOINT [ "./startup.sh" ]

# Expose necessary ports
EXPOSE 26900/tcp
EXPOSE 26900/udp
EXPOSE 26901/udp
EXPOSE 26902/udp
EXPOSE 8080/tcp
EXPOSE 8081/tcp

VOLUME ${STEAMGAMECONFIGDIR} ${STEAMGAMESAVEDIR}

# Setup default environment variables for the server
ENV SEVEN_DAYS_TO_DIE_SERVER_STARTUP_ARGUMENTS "-logfile /dev/stdout -quit -batchmode -nographics -dedicated"
ENV SEVEN_DAYS_TO_DIE_CONFIG_FILE "${STEAMGAMECONFIGDIR}/serverconfig.xml"
ENV SEVEN_DAYS_TO_DIE_TELNET_PORT 8081
ENV SEVEN_DAYS_TO_DIE_TELNET_PASSWORD ""
ENV SEVEN_DAYS_TO_DIE_BRANCH "public"
ENV SEVEN_DAYS_TO_DIE_START_MODE "0"
ENV SEVEN_DAYS_TO_DIE_UPDATE_CHECKING "0"

CMD ["su", "-c",  "./run.sh", "steam"]
