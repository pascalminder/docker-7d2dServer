#!/usr/bin/env bash

# set -e

/startup/setup_users.sh

# su - steam -c "cd ${SCRIPTDIR}; exec ./run.sh "

cd ${SCRIPTDIR}

exec "$@"
