#!/usr/bin/env bash

PUID=${PUID:-911}
PGID=${PGID:-911}

groupmod -o -g "$PGID" steam
usermod -o -u "$PUID" steam

echo "
-------------------------------------
GID/UID

User uid:    $(id -u steam)
User gid:    $(id -g steam)
-------------------------------------
"

chown -R steam:steam /home/steam
chown -R steam:steam ${SCRIPTDIR}
